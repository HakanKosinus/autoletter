from docx import Document
from docx.shared import Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH

import time
import random
import subprocess
import os

from bin.Lettergenerator import Lettergenerator as LG

class Letter:

    @classmethod
    def create(cls, name, createPDF=True):
        if '.docx' not in name:
            name += '.docx'
        Filename = name

        ###### LOGGER
        import logging
        logpath = '/tmp/AutoLetter.log'
        logging.basicConfig(filename=logpath, level=logging.DEBUG, format='%(asctime)s %(message)s')
        ######

        document = Document()

        ###### FONT
        style = document.styles['Normal']
        font = style.font
        font.name = LG.getFont()
        font.size = Pt(random.randint(12, 13))
        ######

        ###### ADDRESS
        p = document.add_paragraph()
        p.add_run(LG.getName())
        p.add_run('\n')
        p.add_run(LG.getStreet())
        p.add_run('\n')
        p.add_run(LG.getCity())
        ######

        ###### SPACING
        p.add_run('\n' * random.randint(4, 7))
        ######

        ###### RECEIVER
        if(random.randint(1, 10) <= 7): # 70% Chance
            p.add_run('ARD-ZDF Deutschlandradio\nBeitragsservice\n50439 Köln')
            p.add_run('\n')
        ######

        ###### DATE
        alignments = [WD_ALIGN_PARAGRAPH.RIGHT, WD_ALIGN_PARAGRAPH.LEFT]
        p = document.add_paragraph()
        p.alignment = random.choice(alignments)

        dateformat = LG.getDateformat()
        p.add_run(time.strftime(dateformat))
        p.add_run('\n')
        ######

        ###### SUBJECT
        p = document.add_paragraph()
        p.add_run(LG.getSubject()).bold = True
        p.add_run('\n' * random.randint(2, 3))
        ######

        ###### ID_Before
        putIDBeforeBody = random.randint(0,1) == 0
        if putIDBeforeBody:
            p.add_run(LG.getIDName())
            p.add_run(LG.getID())
            p.add_run('\n\n')
        ######

        ###### SALUTATION
        p.add_run(LG.getSalutation())
        p.add_run('\n\n')
        ######

        ###### BODY
        p.add_run(LG.getBody())
        p.add_run('\n')
        ######

        ###### ID_After
        if not putIDBeforeBody:
            p.add_run('\n')
            p.add_run(LG.getExtra())
            p.add_run('\n')
        ######

        ###### ENDING
        p.add_run('\n')
        p.add_run(LG.getEnding())
        p.add_run('\n'*random.randint(2,3))
        p.add_run(LG.getName())
        ######

        LG.resetName()

        document.save(Filename)


        ##### Convert to PDF
        if createPDF:
            try:
                args = ['libreoffice', '--headless', '--convert-to', 'pdf', Filename]
                subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=None)
            except FileNotFoundError:
                print("Libreoffice is not installed, can't convert to PDF.")
        
        try:
            os.remove(Filename)
        except:
            print('Could not remove {}'.format(Filename))
        return 0


if __name__ == '__main__':
    Letter.create('demo')
