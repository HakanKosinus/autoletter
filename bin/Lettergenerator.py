import os
import random

class Lettergenerator:

    dataDir = os.path.dirname(os.path.realpath(__file__)) + '/../data'
    name = ''

    def _random_line(fname):
        try:
            e = None
            afile = open(fname, 'r')
            lines = afile.read().splitlines()
            random_line = random.choice(lines)
        except Exception as ex:
            e = ex
        finally:
            afile.close()
            if e is not None :
                raise(e)
            return random_line.strip()

    def getFont():
        fonts = ['Verdana', 'Cantarell', 'Liberation Serif']
        return random.choice(fonts)

    def getDateformat():
        formats = ['%d/%m/%Y', '%d.%m.%Y', '%d-%m-%Y', '%d/%m/%y', '%d.%m.%y', '%d-%m-%y', '%d/%b/%Y', '%d.%b.%Y', '%d-%b-%Y', '%d/%b/%y', '%d.%b.%y', '%d-%b-%y']
        return random.choice(formats)

    def getName():
        if Lettergenerator.name != '':
            return Lettergenerator.name

        fn = Lettergenerator._random_line(Lettergenerator.dataDir + '/sender/firstname.txt')
        ln = Lettergenerator._random_line(Lettergenerator.dataDir + '/sender/lastname.txt')

        Lettergenerator.name = '{} {}'.format(fn,ln)

        return Lettergenerator.name

    def getStreet():
        street = Lettergenerator._random_line(Lettergenerator.dataDir + '/sender/street.txt') + ' ' + str(random.randint(1, 50))
        return street

    def getCity():
        city = Lettergenerator._random_line(Lettergenerator.dataDir + '/sender/city.txt')
        return city

    def getSubject():
        subject =  Lettergenerator._random_line(Lettergenerator.dataDir + '/letter/subject.txt')
        return subject

    def getIDName():
        names = ['Beitragsnummer: ', 'Beitragskonto: ', 'Mitgliedsnummer: ', 'NR. ', 'Kontonr.: ', 'BeitragsID: ']
        return random.choice(names)

    def getID():
        splitElements = [' ','-','.','']
        s = random.choice(splitElements)
        return '{}{}{}{}{}'.format(random.randint(100, 999),s,random.randint(100, 999),s,random.randint(100, 999))

    def getSalutation():
        salutation =  Lettergenerator._random_line(Lettergenerator.dataDir + '/letter/salutation.txt')
        return salutation

    def getEnding():
        ending =  Lettergenerator._random_line(Lettergenerator.dataDir + '/letter/ending.txt')
        return ending

    def getBody():
        timeLimits = ['Tagen', 'Wochen', 'Monaten']

        bs1 = Lettergenerator._random_line(Lettergenerator.dataDir + '/body/bs1.txt')
        bs2 = Lettergenerator._random_line(Lettergenerator.dataDir + '/body/bs2.txt').format(random.randint(150, 950), random.randint(10, 99))
        bs3 = Lettergenerator._random_line(Lettergenerator.dataDir + '/body/bs3.txt')
        bs4 = Lettergenerator._random_line(Lettergenerator.dataDir + '/body/bs4.txt').format(random.randint(3, 8), random.choice(timeLimits))

        return '{} {} {} {}'.format(bs1, bs2, bs3, bs4)

    def getExtra():
        extra = Lettergenerator._random_line(Lettergenerator.dataDir + '/extra/extra.txt')
        return '{} {}'.format(extra, Lettergenerator.getID())

    def resetName():
        Lettergenerator.name = ''
