from Letter import Letter
from flask import Flask, send_file, redirect
from threading import Thread

import os
import string
import random
import time

def deleteFile(fname, timeout=120):
    time.sleep(timeout)
    try:
        os.remove(fname)
    except:
        print("Could not delete {}".format(fname))

app = Flask(__name__)

@app.route('/')
def index():

    name = ''.join(random.choice(string.ascii_letters) for x in range(20))
    Letter.create(name)
    os.rename(name + '.pdf', 'out/' + name + '.pdf')
    return redirect("/out/" + name + '.pdf' , code=302)

@app.route('/out/<filename>', methods=['GET', 'POST'])
def download(filename):
    Thread(target=deleteFile , args=('./out/' + filename,)).start() #delets file after a while
    return send_file('./out/' + filename, as_attachment=True, attachment_filename='Letter.'+filename)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1080, debug=False)
