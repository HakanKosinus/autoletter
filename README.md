# AutoLetter

Generiert Schreiben für den ARD-ZDF-Beitragsservice

### Installation:

`sudo pip3 install -r requirements.txt`

### Ausführen

`python3 Letter.py`

Erzeugt PDF 'demo.pdf'


`python3 Rest.py`

REST-Schnittstelle: Standardmäßig aufzurufen unter **127.0.0.1:1080**

